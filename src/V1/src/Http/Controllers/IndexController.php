<?php namespace V1\Http\Controllers;

use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller;
use Request;
use Response;
use View;

use Common\Http\Controllers\IndexController as MainController;

class IndexController extends MainController
{
    public function test()
    {
        parent::test();
        echo 'Api Call V1 <br />';
    }

}